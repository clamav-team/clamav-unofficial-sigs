# Debian clamav uses different socket and pid files
# If you want to enable the watchdog functionality, copy
# these two lines to your config file and uncomment them:
#clamd_socket="/var/run/clamav/clamd.ctl"
#clamd_pid="/var/run/clamav/clamd.pid"

# Debian clamav uses a different location for the sigs
clam_dbs="/var/lib/clamav"

# Store cached data in /var/cache instead
work_dir="/var/cache/clamav-unofficial-sigs"
ss_dir="$work_dir/ss-dbs"
msrbl_dir="$work_dir/msrbl-dbs"
si_dir="$work_dir/si-dbs"
mbl_dir="$work_dir/mbl-dbs"
add_dir="$work_dir/add-dbs"

# Store persistent data in /var/lib instead
lib_dir="/var/lib/clamav-unofficial-sigs"
config_dir="$lib_dir/configs"
gpg_dir="$lib_dir/gpg-key"

# Turn on logging by default
enable_logging="yes"
log_file_path="/var/log"
log_file_name="clamav-unofficial-sigs.log"

# Silence things for the cron job
curl_silence="yes"
rsync_silence="yes"
gpg_silence="yes"
comment_silence="yes"

# Reload clamav database by default
# But do not run clamdscan if it isn't present
case $reload_opt in
  clamdscan\ *)
    if test -x /usr/bin/clamdscan ; then
      reload_dbs=yes
    else
      reload_dbs=no
      unset reload_opt
    fi
  ;;
  *)
    reload_dbs=yes
  ;;
esac

# Prevent the -r option from removing stuff
pkg_mgr="apt"
pkg_rm="apt-get purge clamav-unofficial-sigs"

# Needed before the script will operate
user_configuration_complete="yes"

# We run the script as the clamav user by default
# so turn off the chown calls, which will fail
unset clam_user
unset clam_group

# Don't touch files in the clam_dbs dir by default
# since they are usually managed by other software.
setmode="no"
